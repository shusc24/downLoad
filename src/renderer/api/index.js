import $ from "jquery";
import Store from "../store/index"
const INNEL_HOST = "http://localhost:3003";
import { Message } from 'element-ui';
let Api = {
    closeServer:function (options) {
        return $.post(`${INNEL_HOST}/api/close`,options).fail(Rs=>{
            Message.error("服务器错误")
        })
    },
    openServer:function (options) {
        return $.post(`${INNEL_HOST}/api/open`,options).fail(Rs=>{
            Message.error("服务器错误")
        })
    },
    getFileData:function (options) {
        return $.post(`${Store.getters.serverPath}/api/getFileDays`,options).fail(Rs=>{
            Message.error("连接失败...")
        })
    }
};
export default Api