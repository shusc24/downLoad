import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/back',
            name: 'downloadingPage',
            component: require('@/components/downloading/index.vue').default,
            meta:{
                keepAlive:true
            }
        },
        {
            path:"/",
            name:"test",
            component:require("@/components/receiveFile/receiveFile.vue").default,
            meta:{
                keepAlive:true
            }
        },
        {
            path:"/history",
            name:"history",
            component:require('@/components/history/index.vue').default,
            meta:{
                keepAlive:true
            }
        },
        {
            path:"/setting",
            name:"setting",
            component:require("@/components/setting/index.vue").default,
            meta:{
                keepAlive:true
            }
        },
        {
            path:"/login",
            name:"login",
            component:require("@/components/login/index.vue").default,
            meta:{
                keepAlive:true
            },
        },
        {
            path:"/judge",
            name:"judge",
            component:require("@/components/judge/index.vue").default,
            meta:{
                keepAlive:true
            },
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})
