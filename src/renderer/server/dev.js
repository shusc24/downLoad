let express = require('express');
let app = express();
let server;
let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials','true');
    next();
};
app.use(allowCrossDomain);
function init() {
    app.post('/api/open', function (req, res) {
        res.send({
            error_code:10001,
            msg:"服务器开启成功!"
        });
    });
    app.post("/api/close",function (req,res) {
        res.send({
            error_code:10001,
            msg:"服务器即将关闭!"
        });
        server.close()
    });
    server = app.listen(3003, function () {
        let host = server.address().address;
        let port = server.address().port;
        console.log('Example app listening at http://%s:%s', host, port);
    });
}
module.exports = init;