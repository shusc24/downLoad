import _ from "lodash";
import Store from "store";

function getStateFromLocak(options) {
    let url = Store.get("url"),id = Store.get("id");
    url?options.url = url:null;
    id?options.id = id:null;
    options.config =_.assign(options.config,{
        autoOpen: Store.get("autoOpen"),
        deskMask: Store.get("deskMask"),
        serverPath:Store.get("serverPath"),
        downLoadPath:Store.get("downLoadPath")
    }) ;
    return options
}

const state = getStateFromLocak({
    url: "D:\\BaiduYunDownload",
    id:0,
    TOKEN:"",
    UserId:"",
    downloadList:[

    ],
    historyList:[

    ],
    config:{
        autoOpen:false,
        deskMask:false,
        openServer:true,
        serverPath:"",
        downLoadPath:""
    }
});

const mutations = {
    SET_URL(state,url){
        Store.set("url",url);
        state.url = url
    },
    ADD_DOWNLOADLIST_ITEM(state,item){
        state.downloadList.push(item);
    },
    SET_DOWNLOADLIST_ITEM_PERCENTAGE(state,num){
        let downloadItem =  _.find(state.downloadList,{
           type:1
        });
        if(downloadItem){
            downloadItem.percentage = num;
        }
    },
    SET_DOWNLOADLIST_ITEM_TYPE(state,type){
        let downloadItem =  _.find(state.downloadList,{
            type:1
        });
        if(downloadItem){
            downloadItem.type = type;
        }
    },
    SET_ID(state){
        let newId = state.id + 1;
        Store.set("id",newId);
        state.id = newId;
    },
    MOVE_DOWNLOADLIST_TO_HISTORYLIST(state){
        let downloadItemIndex = _.findIndex(state.downloadList,{type:1});
        let downloadItem = state.downloadList[downloadItemIndex];
        let newDownloadData = _.cloneDeep(downloadItem);
        state.downloadList.splice(downloadItemIndex,1);
        newDownloadData.type = 0;
        state.historyList.push(newDownloadData);
    },
    SET_AUTOOPEN(state,val){
        Store.set("autoOpen",val);
        state.config.autoOpen = val;
    },
    SET_DESKMASK(state,val){
        Store.set("deskMask",val);
        state.deskMask = val;
    },
    SET_OPENSERVER(state,val){
        state.config.openServer = val;
    },
    SET_TOKEN(state,val){
        state.TOKEN = val;
    },
    SET_USERID(state,val){
        state.UserId = val;
    },
    SET_SERVERPATH(state,val){
        state.config.serverPath = val;
        Store.set("serverPath",val)
    },
    SET_DOWNLOADPATH(state,val){
        state.config.downLoadPath = val;
        Store.set("downLoadPath",val)
    }
};

const getters = {
    url(state){
        return state.url
    },
    downloadList(state){
        return state.downloadList
    },
    id(state){
      return state.id;
    },
    historyList(state){
        return state.historyList
    },
    config(state){
        return state.config
    },
    TOKEN(state){
        return state.TOKEN
    },
    UserId(state){
        return state.UserId
    },
    serverPath(state){
        return state.config.serverPath
    },
    downLoadPath(state){
        return state.config.downLoadPath
    }
};

const actions = {

};

export default {
    state,
    mutations,
    actions,
    getters
}
